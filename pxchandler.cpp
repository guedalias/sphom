#include "pxchandler.h"

void PXCAPI
GestureHandler::OnGesture(PXCGesture::Gesture *data)
{
    PXCtoQt*	wrapper = PXCtoQt::getInstance();
    if ((data->label&PXCGesture::Gesture::LABEL_MASK_SET)<PXCGesture::Gesture::LABEL_SET_CUSTOMIZED) {
        switch (data->label) {
        case PXCGesture::Gesture::LABEL_HAND_WAVE:
            if (!_verbose)
                std::cout << "[Gesture] WAVE" << std::endl;
            wrapper->emitWave();
            break;
        case PXCGesture::Gesture::LABEL_HAND_CIRCLE:
            if (!_verbose)
                std::cout << "[Gesture] CIRCLE" << std::endl;
            wrapper->emitCircle();
            break;
        case PXCGesture::Gesture::LABEL_NAV_SWIPE_LEFT:
            if (!_verbose)
                std::cout << "[Gesture] SWIPE_LEFT" << std::endl;
            wrapper->emitSwipeLeft();
            break;
        case PXCGesture::Gesture::LABEL_NAV_SWIPE_RIGHT:
            if (!_verbose)
                std::cout << "[Gesture] SWIPE_RIGHT" << std::endl;
            wrapper->emitSwipeRight();
            break;
        case PXCGesture::Gesture::LABEL_NAV_SWIPE_UP:
            if (!_verbose)
                std::cout << "[Gesture] SWIPE_UP" << std::endl;
            wrapper->emitSwipeUp();
            break;
        case PXCGesture::Gesture::LABEL_NAV_SWIPE_DOWN:
            if (!_verbose)
                std::cout << "[Gesture] SWIPE_DOWN" << std::endl;
            wrapper->emitSwipeDown();
            break;
        case PXCGesture::Gesture::LABEL_POSE_PEACE:
            if (!_verbose)
                std::cout << "[Pose] PEACE ";
            printStatus(data->active);
            wrapper->emitPeace(data->active);
            break;
        case PXCGesture::Gesture::LABEL_POSE_THUMB_DOWN:
            if (!_verbose)
                std::cout << "[Pose] THUMB_DOWN ";
            printStatus(data->active);
            wrapper->emitThumbDown(data->active);
            break;
        case PXCGesture::Gesture::LABEL_POSE_THUMB_UP:
            if (!_verbose)
                std::cout << "[Pose] THUMB_UP ";
            printStatus(data->active);
            wrapper->emitThumbUp(data->active);
            break;
        case PXCGesture::Gesture::LABEL_POSE_BIG5:
            if (!_verbose)
                std::cout << "[Pose] BIG5 ";
            printStatus(data->active);
            wrapper->emitBig5(data->active);
            break;
        }
    }
}

void
GestureHandler::printStatus(bool active)
{
    if (_verbose)
        return;
    if (!active)
        std::cout << "OFF" << std::endl;
    else
        std::cout << "ON" << std::endl;
}
