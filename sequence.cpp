#include "sequence.h"
#include "sequencename.h"
#include "sequencer.h"

Sequence::Sequence(QGraphicsRectItem* parent) :
    QGraphicsRectItem(parent),
    _focus(false)
{
}

Sequence::~Sequence(void)
{

}

bool
Sequence::init(int x, int y, int w, int h, Sequencer* sequencer)
{
    _sequencer = sequencer;
    setRect(x, y, w, h);
    QPen pen(Qt::transparent, 1);

    _player.init(this);
    _brush.setColor(Qt::GlobalColor::darkGray);
    _brush.setStyle(Qt::SolidPattern);
    _effect.setStrength(0.0f);
    setGraphicsEffect(&_effect);
    setPen(pen);
    return true;
}

void
Sequence::resetFocus()
{
    if (_focus)
    {
        _effect.setStrength(0.0f);
        _focus = false;
        resizeSequence(rect().x(), rect().y(), rect().width(), rect().height());

    }
}

void
Sequence::grabFocus()
{
    if (_focus == false)
    {
        _focus = true;
        _effect.setStrength(0.5f);
        resizeSequence(rect().x(), rect().y(), rect().width(), rect().height());

    }
}

bool
Sequence::setSamples(int index, QList<Sample*>& list)
{
    Sample*             sample;
    QString             name;
    QGraphicsTextItem*  block;
    char                value[100];
    int                 nbr = list.size() + 1;
    int                 Woffset = (rect().width()) / nbr;

    name = "Sequence ";
    name += _itoa(index, value, 10);

    _player.setSamples(list);

    _name = new SequenceName(this);
    _name->setRect(rect().x(), rect().y(), Woffset, rect().height());
    _name->setText(name);
    _name->setBrush(_brush);
    for (int i = 1; i < nbr; ++i)
    {
        sample = new Sample(this);
        sample->setName(list[i - 1]->getName());
        sample->setRect(rect().x() + i * Woffset, rect().y(), Woffset, rect().height());
        sample->setBrush(_brush);
        block = new QGraphicsTextItem(list[i - 1]->getName());
        block->setPos(rect().x() + i * Woffset, rect().y());
        sample->setText(block);
        _items.push_back(sample);
    }
    return true;
}

bool
Sequence::resizeSequence(int x, int y, int w, int h)
{
    QGraphicsTextItem*  block;

    if (_focus)
        setRect(x - 10, y, w + 20, h);
    else
        setRect(x + 10, y, w - 20, h);

    int  nbr = _items.size() + 1;
    int  Woffset = (rect().width()) / nbr;

    _name->setRect(rect().x(), rect().y(), Woffset, rect().height());
    for (int i = 1; i < nbr; ++i)
    {
        _items[i - 1]->setRect(rect().x() + i * Woffset, rect().y(), Woffset, rect().height());
        block = new QGraphicsTextItem(_items[i - 1]->getName());
        block->setPos(rect().x() + i * Woffset, rect().y());
        _items[i - 1]->setText(block);
    }
    return true;
}

void
Sequence::play(void)
{
    _player.startSequence();
}

bool
Sequence::checkplaying(void)
{
    if (_player.isfinish())
        return true;
    return false;
}
