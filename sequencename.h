#ifndef SEQUENCENAME_H
#define SEQUENCENAME_H

#include <QGraphicsRectItem>

class SequenceName : public QGraphicsRectItem
{
private:
    QGraphicsTextItem*   _text;

public:
    SequenceName(QGraphicsRectItem *parent = NULL);
    bool    setText(QString& text);
    void    setRect(qreal x, qreal y, qreal width, qreal height);
signals:
    
public slots:
    
};

#endif // SEQUENCENAME_H
