#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "pxc.h"
#include "pxctoqt.h"
#include "pxchandler.h"
#include "blibli.h"
#include "editor.h"
#include "sequencer.h"
#include "alphascene.h"
#include "myqwidget.h"

class Dialog : public QMainWindow
{
    Q_OBJECT
public:

    Dialog(QWidget *parent = 0);
    virtual ~Dialog(void);
    void    keyPressEvent(QKeyEvent* event);
private:
    PXC				senz;
    PXCtoQt*		wrapper;
    Blibli*         blibli;
    Editor*         editor;
    Sequencer*      sequencer;
    AlphaScene*     alpha;
    QGridLayout*    mainLayout;
    MyQWidget*		_focusWidget;
    QListView          _tree;
    QFileSystemModel   _model;
    
signals:
    
public slots:
    void selectSlot(bool status);
    void changeFocusSlot(int direction);
};

#endif // DIALOG_H
