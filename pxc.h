#ifndef PXC_H
#define PXC_H

#include "pxcsession.h"
#include "pxcsmartptr.h"
#include "pxccapture.h"
#include "util_capture.h"
#include "pxcgesture.h"
#include "pxctoqt.h"
#include "pxchandler.h"
#include <iostream>

class PXC
{
    #define PXCDown -1
    #define PXCUp 1
    #define PXCLeft 2
    #define PXCRight 3
private:
    class pxcHandler: public PXCGesture::Gesture::Handler
    {
        private:
            GestureHandler*	ghandler;
            bool			gesture;
        public:
            virtual void PXCAPI OnGesture(PXCGesture::Gesture *data);
            void				setHandler(GestureHandler* handler);
            bool				hasGesture(void);
            void				setGesture(bool status);
    };
    PXCSmartPtr<PXCSession>		pSession;
    PXCSmartPtr<PXCGesture> 	pGestureDetector;
    UtilCapture*				pCapture;
    PXCSmartPtr<pxcHandler>		myGhandler;
    PXCSmartPtr<GestureHandler> ghandler;
    PXCSmartPtr<AlertHandler> 	ahandler;
    PXCtoQt*					wrapper;
    float						startZPos;
    float						startXPos;
    bool						startGesture;
    bool						pDoubleHere;
private:
    bool	initGestureProfil();
public:
    PXC();
    virtual ~PXC(void);
    bool    init(GestureHandler* gh, AlertHandler* ah);
    bool	exec(void);
    bool	verifGeoNode(void);
};

#endif
