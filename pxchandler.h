#ifndef PXCHANDLER_H
#define PXCHANDLER_H

#include <iostream>
#include "pxcgesture.h"
#include "pxctoqt.h"

class GestureHandler: public PXCGesture::Gesture::Handler
{
private:
    bool	_verbose;

public:
    virtual void PXCAPI OnGesture(PXCGesture::Gesture *data);
    void				printStatus(bool active);
public:
    GestureHandler(bool verbose = false) : _verbose(verbose) {}
};

class AlertHandler:public PXCGesture::Alert::Handler {
public:
    virtual void PXCAPI OnAlert(PXCGesture::Alert *alert) {
        //std::cout << "On Alert" << std::endl;
    }
};


#endif // PXCHANDLER_H
