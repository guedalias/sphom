#include "sequencer.h"
#include "PXCtoQt.h"

Sequencer::Sequencer(QWidget *parent) : QWidget(parent), _parent(parent), _focus(NULL), _playing(NULL)
{
    _isPlaying = false;
}

Sequencer::~Sequencer(void)
{
}

bool
Sequencer::init(void)
{
    QSizePolicy		policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QVBoxLayout*	mainLayout = new QVBoxLayout(this);
    PXCtoQt*		wrapper;


    _timer = new QTimer();
    connect(_timer, SIGNAL(timeout()), this, SLOT(checkPlay()));

    _timer->start(10);
    setLayout(mainLayout);
    _view   = new QGraphicsView(this);
    _scene  = new QGraphicsScene(_view);
    _view->setAutoFillBackground(true);
    _view->setSizePolicy(policy);
    _view->setScene(_scene);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);


    _scene->setBackgroundBrush(Qt::black);

    mainLayout->setMargin(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(_view);
    wrapper = PXCtoQt::getInstance();
    connect(wrapper, SIGNAL(onFingerMove(int)), this, SLOT(fingerSlot(int)));
    connect(wrapper, SIGNAL(onPeace(bool)), this, SLOT(playSlot(bool)));
    QGraphicsView*  view  = new QGraphicsView(this);
    view->setAutoFillBackground(true);
    QGraphicsScene* scene = new QGraphicsScene(view);
    view->setScene(scene);
    QPen    pen(Qt::darkRed, 1, Qt::SolidLine ,Qt::RoundCap, Qt::RoundJoin);
    _pause = new QGraphicsRectItem();
    _pause->setPen(pen);
    _pause->setBrush(Qt::gray);
    _pause->setRect(24, 0, 20, 20);
    _play = new QGraphicsPolygonItem();

    QPolygonF poly;
    poly << QPointF(0, 0) << QPointF(0, 20) << QPointF(16, 10);
    _play->setPolygon(poly);
    _play->setFillRule(Qt::WindingFill);
    QPen    penT(Qt::darkGreen, 1, Qt::SolidLine ,Qt::RoundCap, Qt::RoundJoin);
    _play->setPen(penT);
    _play->setBrush(Qt::green);
    scene->addItem(_play);
    scene->addItem(_pause);
    scene->setBackgroundBrush(Qt::black);

    return true;
}

bool
Sequencer::addSequence(QList<Sample*>& list)
{
    Sequence*   seq = new Sequence();

    seq->init(0, 100 + 66 * _sequences.size(), _view->width() - 4, 64, this);

    _scene->addItem(seq);
    seq->setSamples(_sequences.size() + 1, list);
    _sequences.push_back(seq);
    if (_sequences.size() == 1)
        _focus = seq;
    _scene->setSceneRect(0, 0, _view->width(), _sequences.size() * 66 + 200);
    setSequencesFocus();
    _scene->update(_scene->sceneRect());
    return true;
}

void
Sequencer::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    _scene->setSceneRect(0, 0, _view->width(), _sequences.size() * 66 + 200);
    for (int i = 0; i < _sequences.size(); ++i)
    {
        _sequences[i]->resizeSequence(0, 100 + 66 * i, _view->width() - 4, 64);
        if (_sequences[i] == _focus)
            _view->centerOn(_sequences[i]);
    }
    _scene->update(_view->rect());
}

void
Sequencer::putEvent(QKeyEvent* event)
{
    keyPressEvent(event);
}

void
Sequencer::keyPressEvent(QKeyEvent* event)
{
    int i;

    for (i = 0; i < _sequences.size(); ++i)
    {
        if (_sequences[i] == _focus)
            break;
    }
    if (event->key() == Qt::Key_Up)
    {
        if (i > 0)
            _focus = _sequences[i - 1];
    }
    if (event->key() == Qt::Key_Down)
    {
        if (i < _sequences.size() - 1)
            _focus = _sequences[i + 1];
    }
    if (event->key() == Qt::Key_D)
    {
        _sequences.erase(_sequences.begin() + i);
        if (i < _sequences.size())
            _focus = _sequences[i];
        else if (_sequences.size() > 0)
            _focus = _sequences[i - 1];
        else
            _focus = NULL;
    }
    if (event->key() == Qt::Key_P)
    {
        if (_isPlaying)
        {
            _play->setBrush(Qt::green);
            _pause->setBrush(Qt::gray);
            _isPlaying = false;
        }
        else
        {
            _play->setBrush(Qt::gray);
            _pause->setBrush(Qt::red);
            _isPlaying = true;
            playFocusSequence();
        }
    }
    setSequencesFocus();
}

void
Sequencer::setSequencesFocus(void)
{
    for (int i = 0; i < _sequences.size(); ++i)
    {
        if (_sequences[i] != _focus)
            _sequences[i]->resetFocus();
        else
        {
            _sequences[i]->grabFocus();
            _view->centerOn(_sequences[i]);
        }
    }
}

void
Sequencer::playFocusSequence(void)
{
    if (_focus != NULL && _isPlaying)
    {
        _focus->play();
        _playing = _focus;
    }
}

bool
Sequencer::checkPlay(void)
{
    if (_playing)
    {
        if (_playing->checkplaying())
            playFocusSequence();
    }
    return true;
}

void
Sequencer::setFocusWidget(MyQWidget** wid)
{
    _focusWidget = wid;
}

void
Sequencer::fingerSlot(int direction)
{
    int			key;

    if (*_focusWidget != this)
            return;
    if (direction == -1)
        key = Qt::Key_Down;
    else if (direction == 1)
        key = Qt::Key_Up;
    else if (direction == 2)
        key = Qt::Key_Left;
    else if (direction == 3)
        key = Qt::Key_Right;
    QKeyEvent	event(QEvent::KeyPress, key, 0);
    keyPressEvent(&event);
}

void
Sequencer::playSlot(bool status)
{
    QKeyEvent	event(QEvent::KeyPress, Qt::Key_P, 0);
    if (status)
        keyPressEvent(&event);

}
