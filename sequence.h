#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <QtWidgets>
#include "player.h"
#include "sequencename.h"

class Sequencer;
class Sequence : public QGraphicsRectItem
{
public:
    Sequence(QGraphicsRectItem* parent = NULL);
    virtual ~Sequence(void);

    bool    init(int x, int y, int w, int h, Sequencer* sequencer);
    bool    setSamples(int index, QList<Sample*>& list);
    void    resetFocus(void);
    void    grabFocus(void);
    bool    resizeSequence(int x, int y, int w, int h);
    void    play(void);
    bool    checkplaying(void);
private:
    Sequencer*                  _sequencer;
    QGraphicsColorizeEffect     _effect;
    QBrush                      _brush;
    bool                        _focus;
    SequenceName*               _name;
    QList<Sample*>              _items;
    Player                      _player;


};

#endif // SEQUENCE_H
