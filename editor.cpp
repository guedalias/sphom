#include "editor.h"
#include "PXCtoQt.h"

Editor::Editor(QWidget *parent) : QWidget(parent), _parent(parent)
{
}

Editor::~Editor(void)
{
}

bool
Editor::init(void)
{
    QSizePolicy		policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QVBoxLayout*	mainLayout = new QVBoxLayout(this);
    PXCtoQt*		wrapper;

    setLayout(mainLayout);
    _scene  = new QGraphicsScene(this);
    _scene->setBackgroundBrush(Qt::black);
    _view   = new QGraphicsView(_scene);
    _view->setAutoFillBackground(true);
    _view->setSizePolicy(policy);
    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mainLayout->setMargin(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(_view);
    wrapper = PXCtoQt::getInstance();
    connect(wrapper, SIGNAL(onFingerMove(int)), this, SLOT(fingerSlot(int)));
    connect(wrapper, SIGNAL(onThumbUp(bool)), this, SLOT(selectSlot(bool)));
    connect(wrapper, SIGNAL(onThumbDown(bool)), this, SLOT(deleteSlot(bool)));
    return true;
}

void
Editor::addGroup(QString str)
{
    QDir            current(QDir::currentPath() + "/Samples/" + str);
    QList<QString>* list = new QList<QString>;
    bool            content = false;
    int				curent;
    QStringList entries = current.entryList();

    list->push_back("-Nothing-");
    for (QStringList::ConstIterator entry = entries.begin(); entry != entries.end(); ++entry)
    {
        if (*entry != "." && *entry != "..")
        {
            content = true;
            list->push_back(*entry);
        }
    }
    if (content)
    {
        _manager.addInstrument(list, str);
    }
    curent = _manager.getInstruments().size() - 1;
    _manager.setCurent(curent);
    _manager.getInstruments()[curent]->setCurent(0);
    updateScene();
}


void
Editor::updateScene(void)
{
    int             w = 140;
    int             h = 65;
    QGraphicsItem*  block;
    Sample*         sample;
    Instrument*     instrument;
    int             curent;

    for (int i = 0; i < _manager.getInstruments().size(); ++i)
    {
        curent = _manager.getInstruments()[i]->getCurent();
        for (int j = 0; j < _manager.getInstruments()[i]->getSamples().size(); ++j)
        {
            sample = _manager.getInstruments()[i]->getSamples()[j];
            sample->setRect(i * w, (-j + curent) * h, w, h);
            block = new QGraphicsTextItem(_manager.getInstruments()[i]->getSamples()[j]->getName());
            block->setPos(QPoint(i * w, (-j + curent) * h));
            sample->setText(block);
            sample->resetFocus();
            if (sample->scene() == NULL)
                _scene->addItem(sample);
        }
    }
    curent = _manager.getCurent();
    if (curent >= 0)
    {
        instrument = _manager.getInstruments()[curent];
        curent = instrument->getCurent();
        if (curent >= 0)
        {
            _view->centerOn(instrument->getSamples()[curent]);
            instrument->getSamples()[curent]->grabFocus();
        }
    }
}

void
Editor::resizeEvent(QResizeEvent* event)
{
    int current;
    Instrument* instrument;

    QWidget::resizeEvent(event);
    _scene->setSceneRect(-rect().width(), -rect().height(), rect().width() * 3, rect().height() * 3);
    current = _manager.getCurent();
    if (current >= 0)
    {
        instrument = _manager.getInstruments()[current];
        current = instrument->getCurent();
        if (current >= 0)
            _view->centerOn(instrument->getSamples()[current]);
    }
}

void
Editor::setSequencer(Sequencer* ptr)
{
    _sequencer = ptr;
}

void
Editor::updateList(void)
{
    QList<Sample*>	list;
    Instrument*		instrument;
    int				curent;

    for (int i = 0; i < _manager.getInstruments().size(); ++i)
    {
        instrument = _manager.getInstruments()[i];
        curent = instrument->getCurent();
        if (curent > 0)
            list.push_back(instrument->getSamples()[curent]);
    }
    if (list.size() > 0)
        _sequencer->addSequence(list);
}

void
Editor::deleteCurent(void)
{
    if (_manager.getInstruments().size() <= 0)
        return;
    Instrument* instr = _manager.getInstruments()[_manager.getCurent()];
    for (int i = 0; i < instr->getSamples().size(); ++i)
    {
        delete  instr->getSamples()[i];
    }
    _manager.getInstruments().erase(_manager.getInstruments().begin() + _manager.getCurent());
    _manager.moveLeft();
    if (_manager.getInstruments().size() <= 0)
        _manager.setCurent(-1);
    this->updateScene();
}

void
Editor::putEvent(QKeyEvent* event)
{
    this->keyPressEvent(event);
}

void
Editor::keyPressEvent(QKeyEvent* event)
{
    qDebug() << "Key Editor";
    if (event->key() == Qt::Key_Up)
    {
        if (_manager.getCurent() >= 0)
            _manager.getInstruments()[_manager.getCurent()]->increase();
    }
    if (event->key() == Qt::Key_Down)
    {
        if (_manager.getCurent() >= 0)
            _manager.getInstruments()[_manager.getCurent()]->decrease();
    }
    if (event->key() == Qt::Key_Left)
    {
        _manager.moveLeft();
    }
    if (event->key() == Qt::Key_Right)
    {
        _manager.moveRight();
    }
    if (event->key() == Qt::Key_P)
    {
        this->updateList();
    }
    if (event->key() == Qt::Key_D)
    {
        this->deleteCurent();
    }
    this->updateScene();
}

void
Editor::fingerSlot(int direction)
{
    int			key;

    if (*_focusWidget != this)
            return;
    if (direction == -1)
        key = Qt::Key_Down;
    else if (direction == 1)
        key = Qt::Key_Up;
    else if (direction == 2)
        key = Qt::Key_Left;
    else if (direction == 3)
        key = Qt::Key_Right;
    QKeyEvent	event(QEvent::KeyPress, key, 0);
    keyPressEvent(&event);
}

void
Editor::selectSlot(bool status)
{
    QKeyEvent	event(QEvent::KeyPress, Qt::Key_Space, 0);
    if (status && *_focusWidget == this)
        this->updateList();
}

void
Editor::deleteSlot(bool status)
{
    QKeyEvent	event(QEvent::KeyPress, Qt::Key_D, 0);
    if (status && *_focusWidget == this)
        keyPressEvent(&event);
}

void
Editor::setFocusWidget(MyQWidget** wid)
{
    _focusWidget = wid;
}
