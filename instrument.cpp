#include "instrument.h"

Instrument::Instrument()
{
    _curent = -1;
    qDebug() << "createInst";
}

bool
Instrument::init(QGraphicsScene* scene)
{
    _scene = scene;
    return true;
}

void
Instrument::addSample(QString str, QString dir)
{
    _samples.push_back(new Sample());
    _samples[_samples.size() - 1]->setName(str);
    _samples[_samples.size() - 1]->setDir(dir);
}

QList<Sample*>
Instrument::getSamples(void)
{
    return _samples;
}

void
Instrument::setCurent(int cur)
{
    _curent = cur;
}

int
Instrument::getCurent(void)
{
    return _curent;
}

void
Instrument::increase(void)
{
    if (_curent >= _samples.size() - 1)
        return;
    ++_curent;
}

void
Instrument::decrease(void)
{
    if (_curent <= 0)
        return;
    --_curent;
}
