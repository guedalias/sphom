#include "pxc.h"
#include "pxctoqt.h"

PXCtoQt* PXCtoQt::_singleton = NULL;
bool PXCtoQt::_running = false;

PXCtoQt*
PXCtoQt::getInstance(void)
{
    if (_singleton == NULL)
    {
        _running = true;
        _singleton =  new PXCtoQt();
    }
    return _singleton;
}

void
PXCtoQt::kill(void)
{
    if (_singleton != NULL)
    {
        _running = false;
        _singleton->wait();
        delete _singleton;
        _singleton = NULL;
    }
}

void
PXCtoQt::setSenz(PXC* senz)
{
    _senz = senz;
}

void
PXCtoQt::run(void)
{
    while (_senz->exec() == true && _running)
        ;
    std::cout << "end thread" << std::endl;
}

void
PXCtoQt::setPos(bool hand, float x, float y, float z)
{
    if (hand)
    {
            _xPrimary = x;
            _yPrimary = y;
            _zPrimary = z;
    }
    else
    {
            _xSecondary = x;
            _ySecondary = y;
            _zSecondary = z;
    }
}

float
PXCtoQt::getX(bool hand)
{
    if (hand)
        return _xPrimary;
    else
        return _xSecondary;
}

float
PXCtoQt::getY(bool hand)
{
    if (hand)
        return _yPrimary;
    else
        return _ySecondary;
}

float
PXCtoQt::getZ(bool hand)
{
    if (hand)
        return _zPrimary;
    else
        return _zSecondary;
}
