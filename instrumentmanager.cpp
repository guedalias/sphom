#include "instrumentmanager.h"

InstrumentManager::InstrumentManager()
{
    _curent = -1;
}

bool
InstrumentManager::init(QGraphicsScene* scene)
{
    _scene = scene;
    return true;
}

void
InstrumentManager::addInstrument(QList<QString> *list, QString dir)
{
    _instruments.push_back(new Instrument());
    _instruments[_instruments.size() - 1]->init(_scene);
    for (int i = 0; i < list->size(); ++i)
        _instruments[_instruments.size() - 1]->addSample(list->value(i), dir);
}

QList<Instrument*>&
InstrumentManager::getInstruments(void)
{
    return _instruments;
}

void
InstrumentManager::setCurent(int cur)
{
    _curent = cur;
}

int
InstrumentManager::getCurent(void)
{
    return _curent;
}

void
InstrumentManager::moveLeft(void)
{
    if (_curent <= 0)
        return;
    --_curent;
}

void
InstrumentManager::moveRight(void)
{
    if (_curent >= _instruments.size() - 1)
        return;
    ++_curent;
}
