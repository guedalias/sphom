#ifndef PXCTOQT_H
#define PXCTOQT_H

#include <QObject>
#include <QThread>

class PXC;
class PXCtoQt : public QThread
{
    Q_OBJECT
private:
    static PXCtoQt*	_singleton;
    PXC*			_senz;
    static bool     _running;
    float			_xPrimary;
    float			_yPrimary;
    float			_zPrimary;
    float			_xSecondary;
    float			_ySecondary;
    float			_zSecondary;

private:
    virtual ~PXCtoQt() {}
public:
    static PXCtoQt*	getInstance(void);
    static void		kill(void);
    void	setSenz(PXC* senz);
    void	run(void);
    void	setPos(bool hand, float x, float y, float z);
    float	getX(bool hand);
    float	getY(bool hand);
    float	getZ(bool hand);
    void	emitWave(void) { emit onWave(); }
    void	emitCircle(void) { emit onCircle(); }
    void	emitSwipeLeft(void) { emit onSwipeLeft(); }
    void	emitSwipeRight(void) { emit onSwipeRight(); }
    void	emitSwipeUp(void) { emit onSwipeUp(); }
    void	emitSwipeDown(void) { emit onSwipeDown(); }
    void	emitPeace(bool status) { emit onPeace(status); }
    void	emitThumbDown(bool status) { emit onThumbDown(status); }
    void	emitThumbUp(bool status) { emit onThumbUp(status); }
    void	emitBig5(bool status) { emit onBig5(status); }
    void	emitFingerMove(int direction) { emit onFingerMove(direction); }
    void	emitDoubleFingerMove(int direction) { emit onDoubleFingerMove(direction); }

signals:
    void	onWave(void);
    void	onCircle(void);
    void	onSwipeLeft(void);
    void	onSwipeRight(void);
    void	onSwipeUp(void);
    void	onSwipeDown(void);
    void	onPeace(bool);
    void	onThumbDown(bool);
    void	onThumbUp(bool);
    void	onBig5(bool);
    void	onFingerMove(int);
    void	onDoubleFingerMove(int);

public slots:
    
};

#endif // PXCTOQT_H
