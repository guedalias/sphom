#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <QtWidgets>
#include "sequence.h"
#include "sample.h"
#include "myqwidget.h"

class Sequencer : public QWidget, public MyQWidget
{
    Q_OBJECT
protected:
    virtual void    resizeEvent(QResizeEvent* event);
    void	putEvent(QKeyEvent* event);
    void    keyPressEvent(QKeyEvent* event);
public:
    Sequencer(QWidget* parent = NULL);
    virtual ~Sequencer(void);

    bool    init(void);
    void    setSequencesFocus(void);

    void    playFocusSequence(void);
    void	setFocusWidget(MyQWidget** wid);

public slots:
    bool    checkPlay(void);
    bool    addSequence(QList<Sample *> &list);
    void	fingerSlot(int direction);
    void	playSlot(bool status);

private:
    QTimer*             _timer;
    QWidget*            _parent;
    QGraphicsRectItem*  _pause;
    QGraphicsPolygonItem*   _play;
    QGraphicsView*      _view;
    QGraphicsScene*     _scene;
    QList<Sequence*>    _sequences;
    Sequence*           _focus;
    Sequence*           _playing;
    bool                _isPlaying;
    MyQWidget**			_focusWidget;

};

#endif // SEQUENCER_H
