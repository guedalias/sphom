#include "sequencename.h"


#include <QFont>

SequenceName::SequenceName(QGraphicsRectItem *parent) :
    QGraphicsRectItem(parent),
  _text(NULL)
{
}

bool
SequenceName::setText(QString& text)
{
    QFont serifFont("Times", 10, QFont::Bold);

    if (_text != NULL)
        delete _text;
    _text = new QGraphicsTextItem(this);
    _text->setX(rect().x());
    _text->setY(rect().y());
    _text->setFont(serifFont);
    _text->setPlainText(text);
    return true;
}

void
SequenceName::setRect(qreal x, qreal y, qreal width, qreal height)
{
    QGraphicsRectItem::setRect(x, y, width, height);

    if (_text)
    {
        _text->setX(x);
        _text->setY(y);
    }
}
