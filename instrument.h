#ifndef INSTRUMENT_H
#define INSTRUMENT_H

#include "sample.h"
#include <QtWidgets>

class Instrument
{
public:
    Instrument();

    bool    init(QGraphicsScene* scene);
    void    addSample(QString str, QString dir);
    QList<Sample*>  getSamples(void);
    void    setCurent(int cur);
    int     getCurent(void);
    void    increase(void);
    void    decrease(void);
private:
    QGraphicsScene* _scene;
    QList<Sample*>  _samples;
    int             _curent;
};

#endif // INSTRUMENT_H
