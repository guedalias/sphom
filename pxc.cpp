#include "pxc.h"

PXC::PXC() : pCapture(NULL), pDoubleHere(false)
{
}

PXC::~PXC()
{
    delete pCapture;
}

bool
PXC::init(GestureHandler* gh, AlertHandler* ah)
{
    pxcStatus sts;

    ghandler = gh;
    myGhandler = new pxcHandler();
    myGhandler->setHandler(gh);
    ahandler = ah;
    sts = PXCSession_Create(&pSession);
    if (sts < PXC_STATUS_NO_ERROR)
    {
        std::cerr << "Failed to create the SDK session" << std::endl;
        return false;
    }
    if (initGestureProfil() == false)
        return false;
    sts = pGestureDetector->SubscribeGesture(100,myGhandler);
    if (sts < PXC_STATUS_NO_ERROR)
    {
        std::cerr << "Failed to create the SDK session" << std::endl;
        return false;
    }
    sts = pGestureDetector->SubscribeAlert(ahandler);
    if (sts < PXC_STATUS_NO_ERROR)
    {
        std::cerr << "Failed to create the SDK session" << std::endl;
        return false;
    }
    std::cout << "SDK and Gesture loaded" << std::endl;
    return true;
}

bool
PXC::initGestureProfil()
{
    pxcStatus sts;
    PXCGesture::ProfileInfo gestureProfile;

    sts = pSession->CreateImpl<PXCGesture>(&pGestureDetector);
    if (sts < PXC_STATUS_NO_ERROR)
    {
        std::cerr << "Failed to load any gesture recognition module" << std::endl;
        return false;
    }
    pCapture = new UtilCapture(pSession);
    for (int i=0;;i++)
    {
        sts = pGestureDetector->QueryProfile(i,&gestureProfile);
        if (sts < PXC_STATUS_NO_ERROR)
            break;
        sts = pCapture->LocateStreams(&gestureProfile.inputs);
        if (sts >= PXC_STATUS_NO_ERROR)
            break;
    }
    if (sts<PXC_STATUS_NO_ERROR)
    {
        std::cout << "Failed to locate a capture module" << std::endl;
        return false;
    }
    pGestureDetector->SetProfile(&gestureProfile);
    startGesture = false;
    wrapper = PXCtoQt::getInstance();
    return true;
}

bool
PXC::exec()
{
    pxcStatus sts;
    PXCSmartSPArray sps(2);
    PXCSmartArray<PXCImage> images;
    PXCCapture::VideoStream::Images images2;

    sts = pCapture->ReadStreamAsync(images,&sps[0]);
    if (sts < PXC_STATUS_NO_ERROR)
        return false;
    pCapture->MapImages(0,images,images2);
    sts = pGestureDetector->ProcessImageAsync(images2,&sps[1]);
    if (sts < PXC_STATUS_NO_ERROR && sts != PXC_STATUS_EXEC_ABORTED)
        return false;
    sps.SynchronizeEx();
    sts = sps[0]->Synchronize(0);
    if (sts < PXC_STATUS_NO_ERROR)
        return false;
    if (verifGeoNode() == false)
        return false;
    return true;
}

bool
PXC::verifGeoNode()
{
    pxcStatus sts;
    PXCGesture::GeoNode*	track;
    PXCGesture::GeoNode		primary;
    PXCGesture::GeoNode		secondary;
    bool	primaryHere;
    bool	secondaryHere;

    sts = pGestureDetector->QueryNodeData(0, PXCGesture::GeoNode::LABEL_BODY_HAND_PRIMARY | PXCGesture::GeoNode::LABEL_HAND_FINGERTIP, &primary);
    if (sts == PXC_STATUS_ITEM_UNAVAILABLE)
    {
        primaryHere = false;
        wrapper->setPos(true, -1, -1, -1);
    }
    else
    {
        primaryHere = true;
        if (sts < PXC_STATUS_NO_ERROR)
            return false;
        wrapper->setPos(true, primary.positionWorld.x, primary.positionWorld.y, primary.positionWorld.z);
    }
    sts = pGestureDetector->QueryNodeData(0, PXCGesture::GeoNode::LABEL_BODY_HAND_SECONDARY | PXCGesture::GeoNode::LABEL_HAND_FINGERTIP, &secondary);
    if (sts == PXC_STATUS_ITEM_UNAVAILABLE)
    {
        secondaryHere = false;
        wrapper->setPos(false, -1, -1, -1);
    }
    else
    {
        secondaryHere = true;
        if (sts < PXC_STATUS_NO_ERROR)
            return false;
        wrapper->setPos(false, secondary.positionWorld.x, secondary.positionWorld.y, secondary.positionWorld.z);
    }
    if (!primaryHere)
    {
        if (!secondaryHere)
            return true;
        else
            track = &secondary;
    }
    else
        track = &primary;
    if (track->positionWorld.y < 0.25f)
    {
        if (startGesture == false)
        {
            startZPos = track->positionWorld.z;
            startXPos = track->positionWorld.x;
            myGhandler->setGesture(false);
            startGesture = true;
        }
        if (primaryHere && secondaryHere &&
            primary.positionWorld.y < 0.25f &&
            secondary.positionWorld.y < 0.25f)
            pDoubleHere = true;
    }
    else if (startGesture == true)
    {
        if (!myGhandler->hasGesture())
        {
            float	dz = track->positionWorld.z - startZPos;
            float	dx = track->positionWorld.x - startXPos;
            if (dz < dx)
            {
                if (dz < -dx)
                {
                    if (pDoubleHere)
                    {
                        wrapper->emitDoubleFingerMove(PXCDown);
                        std::cout << "doubleDown" << std::endl;
                    }
                    else
                    {
                        wrapper->emitFingerMove(PXCUp);
                        std::cout << "down" << std::endl;
                    }
                }
                else
                {
                    if (pDoubleHere)
                    {
                        //wrapper->emitDoubleFingerMove(PXCLeft);
                        std::cout << "doubleLeft" << std::endl;
                    }
                    else
                    {
                        wrapper->emitFingerMove(PXCRight);
                        std::cout << "left" << std::endl;
                    }
                }
            }
            else
            {
                if (dz > -dx)
                {
                    if (pDoubleHere)
                    {
                        wrapper->emitDoubleFingerMove(PXCUp);
                        std::cout << "doubleUp" << std::endl;
                    }
                    else
                    {
                        wrapper->emitFingerMove(PXCDown);
                        std::cout << "up" << std::endl;
                    }
                }
                else
                {
                    if (pDoubleHere)
                    {
                        //wrapper->emitDoubleFingerMove(PXCRight);
                        std::cout << "doubleRight" << std::endl;
                    }
                    else
                    {
                        wrapper->emitFingerMove(PXCLeft);
                        std::cout << "right" << std::endl;
                    }

                }
            }
        }
        pDoubleHere = false;
        startGesture = false;
    }
    return true;
}

void PXCAPI
PXC::pxcHandler::OnGesture(PXCGesture::Gesture* data)
{
    gesture = true;
    ghandler->OnGesture(data);
}

void
PXC::pxcHandler::setHandler(GestureHandler* handler)
{
    ghandler = handler;
    gesture = false;
}

bool
PXC::pxcHandler::hasGesture(void)
{
    bool	tmp = gesture;
    if (gesture)
        gesture = false;
    return tmp;
}

void
PXC::pxcHandler::setGesture(bool status)
{
    gesture = status;
}
