#ifndef MYQWIDGET_H
#define MYQWIDGET_H

#include <QWidget>

class MyQWidget
{

public:
    explicit MyQWidget();
    MyQWidget*	getNeighbor(int direction);
    void		setNeighbor(MyQWidget* up, MyQWidget* down);
    virtual void	putEvent(QKeyEvent* event);
private:
    MyQWidget*	up;
    MyQWidget*	down;
signals:
    
public slots:
    
};

#endif // MYQWIDGET_H
