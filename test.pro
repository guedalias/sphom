SOURCES += \
    main.cpp \
    util_captureimpl.cpp \
    util_capture.cpp \
    pxctoqt.cpp \
    pxchandler.cpp \
    pxc.cpp \
    blibli.cpp \
    dialog.cpp \
    editor.cpp \
    sequencer.cpp \
    sequence.cpp \
    sequencename.cpp \
    sample.cpp \
    instrument.cpp \
    instrumentmanager.cpp \
    player.cpp \
    alphascene.cpp \
    myqwidget.cpp
QT += core widgets gui multimedia
CONFIG += console

win32: LIBS += -LC:/PCSDK/lib/x64/ -llibpxc_d \
-lkernel32 \
-luser32 \
-lgdi32 \
-lwinspool \
-lcomdlg32 \
-ladvapi32 \
-lshell32 \
-lole32 \
-loleaut32 \
-luuid \
-lodbc32 \
-lodbccp32 \

INCLUDEPATH += C:/PCSDK/include
DEPENDPATH += C:/PCSDK/include

win32: PRE_TARGETDEPS += C:/PCSDK/lib/x64/libpxc_d.lib

HEADERS += \
    pxctoqt.h \
    pxchandler.h \
    pxc.h \
    util_render.h \
    util_captureimpl.h \
    util_capture.h \
    blibli.h \
    dialog.h \
    editor.h \
    sequencer.h \
    sequence.h \
    sequencename.h \
    sample.h \
    instrument.h \
    instrumentmanager.h \
    player.h \
    alphascene.h \
    myqwidget.h
