#ifndef ALPHASCENE_H
#define ALPHASCENE_H

#include <QtWidgets>
#include "pxctoqt.h"

class AlphaScene : public QWidget
{
    Q_OBJECT
public:
    explicit AlphaScene(QWidget *parent = 0);

    bool    init(void);
    void    resizeEvent(QResizeEvent* event);
    void    setWrapper(PXCtoQt* wrapper);

private:
    QWidget*                _parent;
    QGraphicsScene*         _scene;
    QGraphicsView*          _view;
    QGraphicsEllipseItem*   _circleLeft;
    QGraphicsEllipseItem*   _circleRight;
    PXCtoQt*                _wrapper;
    QTimer*                 _timer;
public slots:
    bool    updateAlpha(void);
    
};

#endif // ALPHASCENE_H
