#include "sample.h"

Sample::Sample(QGraphicsRectItem* parent) : QGraphicsRectItem(parent)
{
    QBrush  brush;

    brush.setColor(Qt::GlobalColor::darkGray);
    brush.setStyle(Qt::SolidPattern);
    setBrush(brush);
    _effect.setStrength(0.0f);
    setGraphicsEffect(&_effect);
    _text = NULL;
}

void
Sample::setName(QString str)
{
    _name = str;
    qDebug() << str;
}

void
Sample::setDir(QString dir)
{
    _dirName = dir;
}

void
Sample::setText(QGraphicsItem* item)
{
    if (_text != NULL)
        delete _text;
    _text = item;
    item->setParentItem(this);
}

QString
Sample::getName(void)
{
    return _name;
}

QString
Sample::getDirName(void)
{
    return _dirName;
}

QGraphicsItem*
Sample::getText(void)
{
    return _text;
}

void
Sample::resetFocus()
{
    if (_focus)
    {
        _effect.setStrength(0.0f);
        _focus = false;
    }
}

void
Sample::grabFocus()
{
    if (_focus == false)
    {
        _focus = true;
        _effect.setStrength(0.5f);
    }
}

