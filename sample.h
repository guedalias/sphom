#ifndef SAMPLE_H
#define SAMPLE_H

#include <QtWidgets>

class Sample : public QGraphicsRectItem
{
public:
    Sample(QGraphicsRectItem* parent = NULL);
    void    setName(QString str);
    void    setDir(QString dir);
    void    setText(QGraphicsItem* item);
    QString getName(void);
    QString getDirName(void);
    QGraphicsItem*  getText(void);
    void	grabFocus(void);
    void	resetFocus(void);
private:
    QString                     _dirName;
    QString 		        	_name;
    QGraphicsItem*  			_text;
    QGraphicsColorizeEffect     _effect;
    bool                        _focus;

};

#endif // SAMPLE_H
