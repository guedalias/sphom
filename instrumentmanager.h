#ifndef INSTRUMENTMANAGER_H
#define INSTRUMENTMANAGER_H

#include "instrument.h"
#include <QtWidgets>

class InstrumentManager
{
public:
    InstrumentManager(void);

    bool    init(QGraphicsScene* scene);
    void    addInstrument(QList<QString>* list, QString dir);
    QList<Instrument*>& getInstruments(void);
    void                setCurent(int cur);
    int                 getCurent(void);
    void                moveLeft(void);
    void                moveRight(void);
private:
    QGraphicsScene*     _scene;
    QList<Instrument*>  _instruments;
    int                 _curent;
};

#endif // INSTRUMENTMANAGER_H
