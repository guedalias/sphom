#ifndef BLIBLI_H
#define BLIBLI_H

#include <QtWidgets>
#include "editor.h"
#include "myqwidget.h"

class Blibli : public QWidget, public MyQWidget
{
    Q_OBJECT
public:
    class	myListView : public QListView
    {
        public:
        void	putEvent(QKeyEvent* event)
        {
            keyPressEvent(event);
        }
    };
    Blibli(QWidget* parent = NULL);
    virtual ~Blibli(void);

    bool                init(void);
    QString             getFocus(void);
//    QFileSystemModel    getModel(void);
    void	putEvent(QKeyEvent* event);
    void    keyPressEvent(QKeyEvent* event);
    void	setEditor(Editor* editor);
    void	setFocusWidget(MyQWidget** wid);
private:
    myListView         _tree;
    QFileSystemModel   _model;
    QString            _focus;
    Editor*			   _editor;
    MyQWidget**		_focusWidget;
private slots:
    void clickSlot(QModelIndex index);
    void fingerSlot(int direction);
};

#endif // BLIBLI_H
