#ifndef EDITOR_H
#define EDITOR_H

#include <QtWidgets>
#include <QList>
#include "sequencer.h"
#include "instrumentmanager.h"
#include "myqwidget.h"

class Editor : public QWidget, public MyQWidget
{
    Q_OBJECT
public:
    Editor(QWidget* parent = NULL);
    virtual ~Editor(void);

    bool    init(void);
    void    addGroup(QString str);
    void    setSequencer(Sequencer* ptr);
    void    clearScene(void);
    void    updateScene(void);
    void    resizeEvent(QResizeEvent* event);
    void	putEvent(QKeyEvent* event);
    void    keyPressEvent(QKeyEvent* event);
    void	setFocusWidget(MyQWidget** wid);
    void	updateList(void);
    void    deleteCurent(void);
private:
    Sequencer*              _sequencer;
    QWidget*                _parent;
    QGraphicsView*          _view;
    QGraphicsScene*         _scene;
    InstrumentManager       _manager;
    MyQWidget**		_focusWidget;
public slots:
    void fingerSlot(int direction);
    void selectSlot(bool status);
    void deleteSlot(bool status);
};

#endif // EDITOR_H
