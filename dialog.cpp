#include "dialog.h"

Dialog::Dialog(QWidget *parent) :
    QMainWindow(parent)
{
    QWidget* central = new QWidget;

    QPalette pal = palette();

    pal.setColor(QPalette::Base, Qt::black);

    pal.setColor(QPalette::Window, Qt::black);
    pal.setColor(QPalette::Button, Qt::green);
    pal.setColor(QPalette::Text, Qt::white);

    pal.setColor(QPalette::Highlight, Qt::green);
    pal.setColor(QPalette::HighlightedText, Qt::green);
    pal.setColor(QPalette::Dark, Qt::green);
    pal.setColor(QPalette::Mid, Qt::green);
    pal.setColor(QPalette::Shadow, Qt::green);

    setCentralWidget(central);
    central->grabKeyboard();

    setMinimumSize(800, 600);

    menuBar()->setPalette(pal);
    menuBar()->addMenu("help");
    setWindowTitle(tr("Deep Shit"));
    setAutoFillBackground(true);

    mainLayout  = new QGridLayout(central);
    editor      = new Editor(central);
    blibli      = new Blibli(central);
    sequencer   = new Sequencer(central);
    alpha       = new AlphaScene(central);

    QSizePolicy     policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QString         style;

    _focusWidget = blibli;
    qDebug() << blibli->styleSheet();
    style = "border: 2px solid orange";
 //   blibli->setStyleSheet(style);
    setPalette(pal);
    sequencer->setAutoFillBackground(true);
    sequencer->setSizePolicy(policy);
    sequencer->init();
    sequencer->setNeighbor(editor, blibli);
    sequencer->setFocusWidget(&_focusWidget);

    editor->setAutoFillBackground(true);
    editor->setSizePolicy(policy);
    editor->init();
    editor->setSequencer(sequencer);
    editor->setNeighbor(blibli, sequencer);
    editor->setFocusWidget(&_focusWidget);

    blibli->setAutoFillBackground(true);
    blibli->setSizePolicy(policy);
    blibli->init();
    blibli->setEditor(editor);
    blibli->setNeighbor(sequencer, editor);
    blibli->setFocusWidget(&_focusWidget);
    blibli->setStyleSheet("border: 2px solid orange;"
                          "background-color: black;"
                          "color: white");

    alpha->setSizePolicy(policy);
    alpha->setEnabled(false);
    alpha->setAttribute(Qt::WA_TransparentForMouseEvents);
    alpha->init();

    mainLayout->setHorizontalSpacing(2);
    mainLayout->setVerticalSpacing(2);

    mainLayout->setColumnStretch(1, 3);
    mainLayout->setColumnStretch(0, 1);
    mainLayout->setRowStretch(0, 6);
    mainLayout->setRowStretch(1, 4);

    mainLayout->addWidget(alpha, 0, 0, 2, 2);
    mainLayout->setMargin(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(blibli, 0, 0, 2, 1);
    mainLayout->addWidget(editor, 0, 1);
    mainLayout->addWidget(sequencer, 1, 1, 1, 1);
    central->setLayout(mainLayout);

    wrapper = PXCtoQt::getInstance();
    if (senz.init(new GestureHandler(true), new AlertHandler()))
    {
        connect(wrapper, SIGNAL(onThumbUp(bool)), this, SLOT(selectSlot(bool)));
        connect(wrapper, SIGNAL(onDoubleFingerMove(int)), this, SLOT(changeFocusSlot(int)));
        wrapper->setSenz(&senz);
        wrapper->start();
    }
    alpha->setWrapper(wrapper);
}

Dialog::~Dialog(void)
{
    PXCtoQt*	wrapper = PXCtoQt::getInstance();
    wrapper->kill();
}

void
Dialog::keyPressEvent(QKeyEvent* event)
{
    qDebug() << "Key Dialog";
    _focusWidget->putEvent(event);
    //this->changeFocusSlot(1);/*TEST*/
}

void
Dialog::selectSlot(bool status)
{
    QKeyEvent	event(QEvent::KeyPress, Qt::Key_Space, 0);
    if (status && _focusWidget == blibli
            )
        keyPressEvent(&event);
}

void
Dialog::changeFocusSlot(int direction)
{
    QWidget*	wid;

    blibli->setStyleSheet("border: 2px solid gray;"
                          "background-color: black;"
                          "color: white");
    editor->setStyleSheet("border: 2px solid gray;"
                          "background-color: black;"
                          "color: white");
    sequencer->setStyleSheet("border: 2px solid gray;"
                             "background-color: black;"
                             "color: white");
    wid = dynamic_cast<QWidget*>(_focusWidget->getNeighbor(direction));

    wid->setStyleSheet("border: 2px solid orange;"
                       "background-color: black;"
                       "color: white");
    wid->setAutoFillBackground(true);
    _focusWidget = dynamic_cast<MyQWidget*>(wid);
}
