#include "blibli.h"
#include "pxctoqt.h"
#include <QSound>
#include "pxc.h"

Blibli::Blibli(QWidget* parent)
    : QWidget(parent)
{
    _editor = NULL;
}

Blibli::~Blibli(void)
{

}

bool
Blibli::init(void)
{
    QSizePolicy     policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QVBoxLayout*    mainLayout = new QVBoxLayout(this);
    QStringList		list;
    QModelIndex		tmpIndex;

    setMaximumWidth(200);
    setLayout(mainLayout);
    setSizePolicy(policy);
    _model.setRootPath("C:/");
    _tree.setModel(&_model);
    _tree.setParent(this);
    _tree.setFocus();
    _tree.setRootIndex(_model.index(QDir::currentPath() + "/Samples"));
    list = QDir(QDir::currentPath() + "/Samples/").entryList();
    if (list.size() > 2)
    {
        tmpIndex = _model.index(QDir::currentPath() + "/Samples/" + list[2]);
        _tree.setCurrentIndex(tmpIndex);
        _focus = _model.fileName(tmpIndex);
    }
    connect(&_tree, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(clickSlot(QModelIndex)));
    connect(PXCtoQt::getInstance(), SIGNAL(onFingerMove(int)), this, SLOT(fingerSlot(int)));
    mainLayout->setMargin(0);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(&_tree);
    return true;
}

void
Blibli::setEditor(Editor* editor)
{
    _editor = editor;
}

void
Blibli::setFocusWidget(MyQWidget** wid)
{
    _focusWidget = wid;
}

QString
Blibli::getFocus(void)
{
    return _focus;
}

void
Blibli::clickSlot(QModelIndex index)
{
    qDebug() << _model.fileName(index);
    _focus = _model.fileName(index);
   // QSound::play("C:/Users/pwet/Downloads/guedalias-sphom-e6f8864af07b/guedalias-sphom-e6f8864af07b/loop xav/" + _model.fileName(index));
}

void
Blibli::fingerSlot(int direction)
{
    QModelIndex	tmp;
    int			key;

    if (*_focusWidget != this)
            return;
    tmp = _model.index(QDir::currentPath() + "/Samples/" + _focus);
    if (direction == PXCDown)
    {
        tmp = tmp.sibling(tmp.row() - 1, 0);
        key = Qt::Key_Up;
    }
    else if (direction == PXCUp)
    {
        tmp = tmp.sibling(tmp.row() + 1, 0);
        key = Qt::Key_Down;
    }
    if (tmp.isValid())
    {
        QKeyEvent	event(QEvent::KeyPress, key, 0);
        _tree.putEvent(&event);
        _focus = _model.fileName(tmp);
    }
}

void
Blibli::putEvent(QKeyEvent* event)
{
    this->keyPressEvent(event);
}

void
Blibli::keyPressEvent(QKeyEvent* event)
{
    qDebug() << "Key Blibli";
    if (event->key() == Qt::Key_Space)
    {
        QString     path = this->getFocus();

        if (path.isEmpty() == false && _editor != NULL)
            _editor->addGroup(path);
    }
}
