#ifndef PLAYER_H
#define PLAYER_H

#include <QAudioOutput>
#include <QAudio>
#include <QFile>
#include <QObject>
#include <QBuffer>
#include <QSound>

class Sequence;
class Sample;
class Player : public QObject
{
    Q_OBJECT
public:
    Player();
    virtual ~Player(void);

    bool    init(Sequence* parent);
    bool    startSequence(void);
    bool    setSamples(QList<Sample*>& list);
    bool    isfinish(void);

protected:
    QList<QSound*>  _sounds;
//    Sequence*         _parent;
//    QList<QSound*>    _samples;
//    QList<QBuffer*>   _sources;
//    QAudioFormat      _format;
};

#endif // PLAYER_H
