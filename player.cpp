#include "player.h"
#include "sequence.h"
#include "sample.h"

Player::Player() : QObject(NULL)
{
}

Player::~Player(void)
{

}

bool
Player::init(Sequence* parent)
{
    return true;
}

bool
Player::setSamples(QList<Sample*>& list)
{
    QSound* sound;
    QString         name;
    QString         dirName;
    for (int i = 0; i < list.size(); ++i)
    {
        name    = list[i]->getName();
        dirName = list[i]->getDirName();
        sound = new QSound(QDir::currentPath() + "/Samples/" + dirName + "/" + name);
        _sounds.push_back(sound);
    }
    return true;
}

bool
Player::startSequence(void)
{
    qDebug() << "Start played";
    for (int i = 0; i < _sounds.size(); ++i)
    {
        _sounds[i]->play();
    }
    return true;
}

bool
Player::isfinish(void)
{
    if (_sounds[0]->isFinished())
        return true;
    return false;
}
