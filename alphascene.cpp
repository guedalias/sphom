#include "alphascene.h"

AlphaScene::AlphaScene(QWidget *parent) :
    QWidget(parent),
    _parent(parent)
{
}

bool
AlphaScene::init(void)
{
     QSizePolicy       policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
     QVBoxLayout*      mainLayout = new QVBoxLayout(this);

     setLayout(mainLayout);

     _view   = new QGraphicsView(this);
     _scene  = new QGraphicsScene(_view);

     _view->setSizePolicy(policy);
     _view->setScene(_scene);
     _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
     _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

     _scene->setSceneRect(_parent->rect());

     mainLayout->setMargin(0);
     mainLayout->setContentsMargins(0, 0, 0, 0);
     mainLayout->addWidget(_view);

     QPalette   pal = _view->palette();

     pal.setColor(QPalette::Base, Qt::transparent);
     _view->setPalette(pal);


     QPen   pen(Qt::darkBlue, 16);
     _view->setAutoFillBackground(false);
     setAutoFillBackground(false);
     _circleLeft = new QGraphicsEllipseItem();
     _circleLeft->setOpacity(0.0f);
     _circleLeft->setPen(pen);
     _circleLeft->setRect(0, 0, 64, 64);
     _circleLeft->setBrush(Qt::transparent);
     _scene->addItem(_circleLeft);

     _circleRight = new QGraphicsEllipseItem();
     _circleRight->setOpacity(0.0f);
     _circleRight->setPen(pen);
     _circleRight->setRect(200, 200, 64, 64);
     _circleRight->setBrush(Qt::transparent);
     _scene->addItem(_circleRight);

     setAutoFillBackground(false);
     return true;
}

void
AlphaScene::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
    _scene->setSceneRect(rect());

    _scene->update(_view->rect());

}

void
AlphaScene::setWrapper(PXCtoQt* wrapper)
{
    _timer = new QTimer;

    connect(_timer, SIGNAL(timeout()), this, SLOT(updateAlpha()));
    _wrapper = wrapper;

    _timer->start(30);
}

bool
AlphaScene::updateAlpha(void)
{
    float   Lx = 0.4f - (_wrapper->getX(true) + 0.2f);
    float   Ly = _wrapper->getY(true);
    float   Lz = 0.3f - (_wrapper->getZ(true) + 0.15f);

    float   Rx = 0.4f - (_wrapper->getX(false) + 0.2f);
    float   Ry = _wrapper->getY(false);
    float   Rz = 0.3f - (_wrapper->getZ(false) + 0.15f);

    float   w = 0.4f;
    float   h = 0.3f;
    float   y = 0.25f;

    if (Ry > 0.0f)
    {
        _circleRight->setOpacity(1 / (1 + abs(y - Ry) * 20));
        _circleRight->setRect(Rx * rect().width() / w, Rz * rect().height() / h, 64, 64);
    }
    else
        _circleRight->setOpacity(0.0f);
    if (Ly > 0.0f)
    {
        _circleLeft->setOpacity(1 / (1 + abs(y - Ly) * 20));
        _circleLeft->setRect(Lx * rect().width() / w, Lz * rect().height() / h, 64, 64);
    }
    else
        _circleLeft->setOpacity(0.0f);
    _scene->update(_view->rect());
    return true;
}
